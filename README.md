# hello-express

This project contains REST API with following features:
- welcome endpoint
- multiply endpoint

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.


### Dependencies
First install the dependencies:

```sh
npm i
```
### Start the service
Start the REST API service:

`node app.js`

Or in the latest NODEJS version:

`node --watch app.js`

<img src="https://css-tricks.com/wp-content/uploads/2016/01/choose-markdown.jpg">